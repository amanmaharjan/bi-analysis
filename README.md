# Medicine `B` & `I` Analysis #

The exercise is in spring boot. It uses gradle, thymeleaf, google charts and jquery.

So firstly,

1. Requirements: 
    * The Java Development Kit (JDK), version 1.8 or higher.
    * A Gradle distribution, version 4.10 or better.
2. Setup git and clone project from the repository. `git clone https://amanmaharjan@bitbucket.org/amanmaharjan/bi-analysis.git`. Or download repository.
3. Run the project with IDE or `./gradlew bootrun` and visit http://localhost:8080 (**NOTE: HTTP not HTTPS**).

     
## Task description ##
1. Imagine we have a database that has data about individual patients, and what drugs they have purchased, and what day they have purchased it.
    It contains the drug purchases for the drugs B and I. You will find a list of PatientID, the Drug taken (B or I) and the day of purchase.
    i.e. it is a list showing, that a particular patient bought drug B or I on a particular day.
    In regards to day of purchase, if one record shows "1" for day of purchase, and another record shows "9", then it means the second record was purchased 8 days after the first. 
 
2. We are doing an audit of the patients to see which patients are taking B and I together.
    You can assume that each time a patient purchases the drug B, they are on treatment with drug B for the next 30 days. 
    Each time a patient purchases drug I, they are on treatment with drug I for the next 90 days.
 
3. Some rules and categories for patients taking drug B and I.
    * **Patients that violated** by clearly taking B and I together. You can tell that the patients are taking both drugs together. I.e. the purchases overlap. Example: their pattern may look something like B, I, I, B, B, B, I etc
    * **Patients that did not violate**, because they never took B and I together. They may have taken both B and I at some stage, but there is a gap between using the 2 drugs. Example: the pattern will look something like B, B, B, a gap , I, I, I etc. (Remember, B lasts for 30 days, and I lasts for 90 days. So a gap means they are not on any medication)
    * **Patients that switched** into B or switched into I. Here using 30 days for B and 90 days for I, it will look like the patients have taken both drugs for a time period, but actually they shouldn't be interpreted as taking both drugs, and should be interpreted as switching drugs from B to I or from I to B. The pattern will look something like I, I, I, B, B, B or I, I, I, B, B, B, B, B etc. Eg it will only overlap for that last purchase before switching drugs.
        * Note that until the patient comes out clean(not on treatment on either drug), only one switch is allowed. A streak of I, I, I, B, B, B, B, I, I is a violation, while I, I, I, B, B, a gap, I, I, B, B is 2 separate cases of I to B switches.
    * **Patients that seem to trial** B/I as a once off - the pattern will look something like I, I, I, I, I, B, I, I, I, I or B, B, B, B, B, I, B, B, B, B. Because they are only trialing the other drug (took only once), we won't count them as taking both drugs at once.
        * Note: similar to switches, a patient is only allowed for one trial during one streak. I.e. I, I, B, I, I, I, I, B, I is considered a violation rather than trials. The patient can have another trial after he/she comes out clean.
    * Switch, trial and violation does not carry over to the next streak once the patient comes out clean. I.e. I, I, I, B, B, a gap, B, I, I is not a violation. Rather, it is an I to B switch followed by a B to I switch. 
    * A case of single alternative drug at the end of a streak is considered a switch. I.e. I, I, I, B is considered a I to B switch rather than B trial, even if the patient only took B once. Similarly, a single purchase of different drug at the beginning of a streak is also considered a switch. I.e. I, B, B, B is an I to B switch.
    * One patient will only be put into one category. When a patient falls under more than one category, the actual category he/she belongs to should be decided as follows:
        1. If at any point of the purchase history there is a violation, the patient is considered violation.
        2. If throughout the purchase history, the patient did not have any switch, trial or violation, then he/she is considered never took B and I together.
        3. If the patient has more than one case of switch and/or trial, he/she's category is the first switch/trial category he/she had. For example, I, I, I, B, B, a gap, B, B, I, B has an I to B switch, followed by a I trial. The patient is considered I to B switch, since it happend before I trial.


## Testing

You can run the tests with `./gradlew test`.

## Overview
Project **Medication B & I Analysis** analyse medication purchased/used by each patient along with day of consumption. 
It has some rules as mentioned in Task Description. Following those rules, patients are divided into several categories.
The purpose of this application is to show the diversity of patients by their usage of medication.

CSV data is used for patient and medication information. In this application, `data.csv` is the source of data set. It 
includes patient id, medication name and day of purchase respectively. DTOs are used for analysis result from which the 
front end used to display data in chart and table.

## Technologies
1. Client Side:
    * Thymeleaf for resources like css and scripts.
    * Google charts to display reports in chart and table.
    * JQuery for rest call from front end to backend.
    
2. Server Side:
    * Spring boot for backend architecture including rest mapping.
    * Gradle framework for plugins and dependencies.
    * Java 8 stream
    * Custom Api response for standard output to frontend.
    

## API

* **GET** /api/bi/analysis

## Future Improvements

* Database integration
* Exporting reports to pdf, excel etc.
* Notification and/or email service as per report analysis.

