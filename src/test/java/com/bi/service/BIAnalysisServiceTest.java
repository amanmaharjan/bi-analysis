package com.bi.service;

import com.bi.dao.PurchaseRecordDAO;
import com.bi.payload.ApiResponse;
import com.bi.payload.dto.AnalysisResult;
import com.bi.payload.dto.PurchaseRecord;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.Collections;
import java.util.Objects;

import static com.bi.enums.PatientType.*;
import static org.mockito.Mockito.when;

/**
 * Created with IntelliJ IDEA.
 * User: pc
 * Date: 3/4/20
 * Time: 16:16
 * To change this template use File | Settings | File Templates.
 */
@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class BIAnalysisServiceTest {

    @Mock
    private PurchaseRecordDAO purchaseRecordDAO;

    private BIAnalysisService biAnalysisService;

    @Before
    public void setUp() {
        biAnalysisService = new BIAnalysisService(purchaseRecordDAO);
    }

    @Test
    public void shouldProbablyDoSomeTestingHereIsABrokenExample() {
        setupPatients(
                new PurchaseRecord(1, "B", 1),
                new PurchaseRecord(100, "I", 1),
                new PurchaseRecord(40, "B", 2),
                new PurchaseRecord(60, "I", 2),
                new PurchaseRecord(41, "I", 3),
                new PurchaseRecord(10, "B", 3),
                new PurchaseRecord(190, "B", 4),
                new PurchaseRecord(97, "I", 4),
                new PurchaseRecord(6, "I", 4),
                new PurchaseRecord(221, "I", 4),
                new PurchaseRecord(60, "I", 5),
                new PurchaseRecord(70, "I", 5),
                new PurchaseRecord(40, "B", 6),
                new PurchaseRecord(99, "B", 7),
                new PurchaseRecord(8, "B", 7),
                new PurchaseRecord(192, "I", 7),
                new PurchaseRecord(223, "B", 7),
                new PurchaseRecord(315, "B", 7),
                new PurchaseRecord(50, "I", 8),
                new PurchaseRecord(140, "B", 8)
        );

        ApiResponse<?> apiResponse = biAnalysisService.performBIAnalysis();
        AnalysisResult result = (AnalysisResult) apiResponse.getData();

        assert Objects.equals(apiResponse.getMessage(), "BI Analysis completed.");

        assert result.getTotal(VALID_BI_SWITCH) == 2;
        Assert.assertEquals(result.getPatientIds(VALID_BI_SWITCH), Arrays.asList(1, 3));

        assert result.getTotal(VALID_IB_SWITCH) == 1;
        Assert.assertEquals(result.getPatientIds(VALID_IB_SWITCH), Collections.singletonList(8));

        assert result.getTotal(VIOLATED) == 1;
        Assert.assertEquals(result.getPatientIds(VIOLATED), Collections.singletonList(2));

        assert result.getTotal(VALID_B_TRIAL) == 1;
        Assert.assertEquals(result.getPatientIds(VALID_B_TRIAL), Collections.singletonList(4));

        assert result.getTotal(VALID_I_TRIAL) == 1;
        Assert.assertEquals(result.getPatientIds(VALID_I_TRIAL), Collections.singletonList(7));

        assert result.getTotal(VALID_NO_COMED) == 2;
        Assert.assertEquals(result.getPatientIds(VALID_NO_COMED), Arrays.asList(5, 6));

    }

    private void setupPatients(PurchaseRecord... values) {
        when(purchaseRecordDAO.allPurchaseRecords()).thenReturn(Arrays.asList(values));
    }

}
