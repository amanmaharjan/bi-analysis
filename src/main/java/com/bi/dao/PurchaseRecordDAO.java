package com.bi.dao;

import com.bi.payload.dto.PurchaseRecord;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: pc
 * Date: 3/3/20
 * Time: 15:47
 * To change this template use File | Settings | File Templates.
 */
public interface PurchaseRecordDAO {

    List<PurchaseRecord> allPurchaseRecords();

}
