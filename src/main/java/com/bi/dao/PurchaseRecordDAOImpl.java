package com.bi.dao;

import com.bi.exception.LocalisedException;
import com.bi.payload.dto.PurchaseRecord;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: pc
 * Date: 3/3/20
 * Time: 15:50
 * To change this template use File | Settings | File Templates.
 */
@Repository
public class PurchaseRecordDAOImpl implements PurchaseRecordDAO {

    @Value("${mock.data.path}")
    private String mockDataPath;

    @Override
    public List<PurchaseRecord> allPurchaseRecords() {
        List<PurchaseRecord> records = new ArrayList<>();
        try {
            for (Object line : FileUtils.readLines(new File(mockDataPath))) {
                final String[] fields = String.valueOf(line).split(",");
                if(fields.length != 3) {
                    throw new LocalisedException("Data must contain day of purchase, medication name and patient id.");
                }
                final int day = Integer.parseInt(fields[0]);
                final String medication = fields[1];
                final int patientId = Integer.parseInt(fields[2]);
                records.add(new PurchaseRecord(day, medication, patientId));
            }
        } catch (IOException e) {
            throw new LocalisedException(e.getMessage(), e);
        }
        return records;
    }

}
