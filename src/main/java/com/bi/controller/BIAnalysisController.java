package com.bi.controller;

import com.bi.payload.ApiResponse;
import com.bi.service.BIAnalysisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created with IntelliJ IDEA.
 * User: pc
 * Date: 3/3/20
 * Time: 15:33
 * To change this template use File | Settings | File Templates.
 */
@RestController
@RequestMapping(value = "/api")
public class BIAnalysisController {

    private final BIAnalysisService biAnalysisService;

    @Autowired
    public BIAnalysisController(BIAnalysisService biAnalysisService) {
        this.biAnalysisService = biAnalysisService;
    }

    @GetMapping(value = "/bi/analysis")
    @ResponseBody
    public ResponseEntity<?> analysis() {
        ApiResponse<?> apiResponse = biAnalysisService.performBIAnalysis();
        Boolean successfulResponse = apiResponse.getSuccess();
        if(Boolean.TRUE.equals(successfulResponse)) {
            return new ResponseEntity<>(apiResponse, HttpStatus.OK);
        }
        return new ResponseEntity<>(apiResponse, HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
