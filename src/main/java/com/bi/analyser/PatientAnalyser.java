package com.bi.analyser;

import com.bi.enums.MedicineType;
import com.bi.payload.dto.PurchaseRecord;
import com.bi.payload.dto.Record;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created with IntelliJ IDEA.
 * User: pc
 * Date: 3/3/20
 * Time: 16:02
 * To change this template use File | Settings | File Templates.
 */
public class PatientAnalyser {

    private Integer patientId;
    private String switchFromTo;
    private String trialTo;
    private Boolean isViolated;
    private List<PurchaseRecord> purchaseRecords;

    public PatientAnalyser(Integer patientId, List<PurchaseRecord> purchaseRecords) {
        this.patientId = patientId;
        this.purchaseRecords = orderByDay(purchaseRecords);
        checkForTrial();
    }

    private void setSwitchFromTo(Record record) {
        if(switchFromTo == null) {
            setSwitchFromTo(record.getOldRecord().getPurchaseRecord().getMedication() + "_TO_" +
                    record.getPurchaseRecord().getMedication());
            record.setAlreadySwitched(true);
        }
    }

    public Integer getPatientId() {
        return patientId;
    }

    public String getSwitchFromTo() {
        return switchFromTo;
    }

    public String getTrialTo() {
        return trialTo;
    }

    public Boolean getViolated() {
        return isViolated;
    }

    private void setSwitchFromTo(String switchFromTo) {
        if(this.switchFromTo == null) {
            this.switchFromTo = switchFromTo;
        }
    }

    private void setTrialTo(String trialTo) {
        if(this.trialTo == null) {
            this.trialTo = trialTo;
        }
    }

    private void setIsViolated(Boolean isViolated) {
        this.isViolated = isViolated;
    }

    public void analyse() {
        Record first = new Record(purchaseRecords.get(0));
        first.process();
        for(int index = 1; index < purchaseRecords.size(); index++) {
            PurchaseRecord eachRecord = purchaseRecords.get(index);
            Record _this = new Record(eachRecord);
            _this.setOldRecord(first);
            _this.process();
            if(_this.isSwitched()) {
                setSwitchFromTo(_this);
            }
            setIsViolated(_this.getAlreadySwitched() || (_this.isSwitched() && _this.isOverlapped()));
            first = _this;
        }
    }

    private void checkForTrial() {
        if(purchaseRecords.size() == 1) {
            return;
        }
        String firstMedicine = purchaseRecords.get(0).getMedication();
        Map<String, Long> medicineTakenMap = purchaseRecords.stream().map(PurchaseRecord::getMedication)
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
        List<String> medicineTakenList = purchaseRecords.stream().map(PurchaseRecord::getMedication).collect(Collectors.toList());
        String nextMedicine = MedicineType.valueOf(firstMedicine).next();
        if(medicineTakenMap.containsKey(nextMedicine) && medicineTakenMap.get(nextMedicine) == 1 &&
                medicineTakenList.lastIndexOf(nextMedicine) != 0 && purchaseRecords.size() > 2) {
            setTrialTo(nextMedicine);
        }
    }

    private List<PurchaseRecord> orderByDay(List<PurchaseRecord> purchaseRecords) {
        return purchaseRecords.stream().sorted(Comparator.comparing(PurchaseRecord::getDay)).collect(Collectors.toList());
    }

}
