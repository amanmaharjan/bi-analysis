package com.bi.service;

import com.bi.analyser.PatientAnalyser;
import com.bi.dao.PurchaseRecordDAO;
import com.bi.enums.ChangeResultType;
import com.bi.enums.PatientType;
import com.bi.exception.LocalisedException;
import com.bi.payload.ApiResponse;
import com.bi.payload.dto.AnalysisResult;
import com.bi.payload.dto.PurchaseRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.bi.enums.PatientType.VALID_NO_COMED;
import static com.bi.enums.PatientType.VIOLATED;

/**
 * Created with IntelliJ IDEA.
 * User: pc
 * Date: 3/3/20
 * Time: 15:44
 * To change this template use File | Settings | File Templates.
 */
@Service
public class BIAnalysisService {

    private final PurchaseRecordDAO purchaseRecordDAO;

    @Autowired
    public BIAnalysisService(PurchaseRecordDAO purchaseRecordDAO) {
        this.purchaseRecordDAO = purchaseRecordDAO;
    }

    public ApiResponse<?> performBIAnalysis() {
        try {
            List<PurchaseRecord> purchaseRecords = purchaseRecordDAO.allPurchaseRecords();
            Map<Integer, List<PurchaseRecord>> patientMap = groupByPatientId(purchaseRecords);
            List<PatientAnalyser> analyserList = new ArrayList<>();
            patientMap.forEach((patientId, records) -> {
                PatientAnalyser analyzer = new PatientAnalyser(patientId, records);
                analyzer.analyse();
                analyserList.add(analyzer);
            });

            AnalysisResult result = PatientType.getEmptyResult();
            getResultFromAnalyzer(analyserList, result);
            return new ApiResponse<>(true,"BI Analysis completed.", result);
        } catch (LocalisedException e) {
            e.getErrorMessages().add(0, "Failed to perform BI analysis.");
            return new ApiResponse<>(false, "Analysis operation failed.", e);
        } catch (RuntimeException e) {
            return new ApiResponse<>(false, "Analysis operation failed.", e);
        }
    }

    private Map<Integer, List<PurchaseRecord>> groupByPatientId(List<PurchaseRecord> purchaseRecords) {
        return purchaseRecords.stream().collect(Collectors.groupingBy(PurchaseRecord::getPatientId));
    }

    private void getResultFromAnalyzer(List<PatientAnalyser> analyzerList, AnalysisResult result) {
        analyzerList.forEach(analyzer -> {
            PatientType patientType;
            if(analyzer.getTrialTo() != null) {
                patientType = ChangeResultType.valueOf(analyzer.getTrialTo()).getValue();
            } else if(analyzer.getSwitchFromTo() != null) {
                Boolean isViolated = analyzer.getViolated();
                if(Boolean.TRUE.equals(isViolated)) {
                    patientType = VIOLATED;
                } else {
                    patientType = ChangeResultType.valueOf(analyzer.getSwitchFromTo()).getValue();
                }
            } else {
                patientType = VALID_NO_COMED;
            }
            result.putTotal(patientType, result.getTotal(patientType) + 1);
            result.addPatientToPatientType(patientType, analyzer.getPatientId());
        });
    }

}
