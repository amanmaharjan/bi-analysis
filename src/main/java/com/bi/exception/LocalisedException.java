package com.bi.exception;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: pc
 * Date: 3/4/20
 * Time: 20:20
 * To change this template use File | Settings | File Templates.
 */
public class LocalisedException extends RuntimeException {

    private final List<String> errorMessages;

    public LocalisedException(String message, Throwable cause) {
        super(cause.getMessage(), cause);
        errorMessages = new ArrayList<>();
        errorMessages.add(message);
    }

    public LocalisedException(String message) {
        super();
        errorMessages = new ArrayList<>();
        errorMessages.add(message);
    }

    public List<String> getErrorMessages() {
        return errorMessages;
    }

}
