package com.bi.enums;

/**
 * Created with IntelliJ IDEA.
 * User: pc
 * Date: 3/3/20
 * Time: 15:42
 * To change this template use File | Settings | File Templates.
 */
public enum MedicineType {

    B(30),
    I(90);

    private int days;

    MedicineType(int days) {
        this.days = days;
    }

    public int getDays() {
        return days;
    }

    public String next() {
        if(this == MedicineType.B) {
            return MedicineType.I.name();
        } else {
            return MedicineType.B.name();
        }
    }

}
