package com.bi.enums;

import com.bi.payload.dto.AnalysisResult;

/**
 * Created with IntelliJ IDEA.
 * User: pc
 * Date: 3/3/20
 * Time: 15:40
 * To change this template use File | Settings | File Templates.
 */
public enum PatientType {

    VIOLATED("Patients that violated by taking B and I together."),
    VALID_NO_COMED("Patients that did not violate, because they never took B and I together."),
    VALID_BI_SWITCH("Patients that did not violate, because they switched from B to I."),
    VALID_IB_SWITCH("Patients that did not violate, because they switched from I to B."),
    VALID_I_TRIAL("Patients that did not violate, because they simply trialled I during B."),
    VALID_B_TRIAL("Patients that did not violate, because they simply trialled B during I.");

    private String name;

    PatientType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public static AnalysisResult getEmptyResult() {
        AnalysisResult result = new AnalysisResult();
        result.putTotal(VIOLATED, 0);
        result.putTotal(VALID_NO_COMED, 0);
        result.putTotal(VALID_BI_SWITCH, 0);
        result.putTotal(VALID_IB_SWITCH, 0);
        result.putTotal(VALID_I_TRIAL, 0);
        result.putTotal(VALID_B_TRIAL, 0);
        return result;
    }

}
