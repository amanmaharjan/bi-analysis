package com.bi.enums;

/**
 * Created with IntelliJ IDEA.
 * User: pc
 * Date: 3/3/20
 * Time: 15:42
 * To change this template use File | Settings | File Templates.
 */
public enum ChangeResultType {

    B_TO_I(PatientType.VALID_BI_SWITCH),
    I_TO_B(PatientType.VALID_IB_SWITCH),
    B(PatientType.VALID_B_TRIAL),
    I(PatientType.VALID_I_TRIAL);

    private PatientType value;

    public PatientType getValue() {
        return value;
    }

    ChangeResultType(PatientType value) {
        this.value = value;
    }

}
