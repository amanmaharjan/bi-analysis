package com.bi.payload;

/**
 * Created with IntelliJ IDEA.
 * User: pc
 * Date: 3/4/20
 * Time: 22:29
 * To change this template use File | Settings | File Templates.
 */
public class ApiResponse<T> {

    private Boolean success;
    private String message;
    private T data;

    public ApiResponse(Boolean success, String message, T data) {
        this.success = success;
        this.message = message;
        this.data = data;
    }

    public Boolean getSuccess() {
        return success;
    }

    public String getMessage() {
        return message;
    }

    public T getData() {
        return data;
    }

}
