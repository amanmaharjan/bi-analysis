package com.bi.payload.dto;

import com.bi.enums.PatientType;

import java.util.*;

import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toMap;

/**
 * Created with IntelliJ IDEA.
 * User: pc
 * Date: 3/3/20
 * Time: 15:36
 * To change this template use File | Settings | File Templates.
 */
public class AnalysisResult {

    private Map<PatientType, Integer> patients;

    private Map<PatientType, List<Integer>> patientDetails = new EnumMap<>(PatientType.class);

    public AnalysisResult() {
        patients = new EnumMap<>(PatientType.class);
    }

    public void putTotal(PatientType patientType, int total) {
        this.patients.put(patientType, total);
    }

    public Integer getTotal(PatientType patientType) {
        return this.patients.get(patientType);
    }

    public Map<PatientType, Integer> getPatients() {
        return patients;
    }

    public void setPatients(Map<PatientType, Integer> patients) {
        this.patients = patients;
    }

    public Map<PatientType, String> getPatientTypeNameMap() {
        return Arrays.stream(PatientType.values()).collect(toMap(identity(), PatientType::getName));
    }

    public void addPatientToPatientType(PatientType patientType, Integer patientId) {
        if(!patientDetails.containsKey(patientType)) {
            patientDetails.put(patientType, new ArrayList<>());
        }
        patientDetails.get(patientType).add(patientId);
    }

    public List<Integer> getPatientIds(PatientType patientType) {
        return this.patientDetails.get(patientType);
    }

}
