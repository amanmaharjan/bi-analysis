package com.bi.payload.dto;

import com.bi.enums.MedicineType;

import java.util.Objects;

/**
 * Created with IntelliJ IDEA.
 * User: pc
 * Date: 3/3/20
 * Time: 16:03
 * To change this template use File | Settings | File Templates.
 */
public class Record {

    private int validityDays;

    private Boolean alreadySwitched = false;

    private PurchaseRecord purchaseRecord;

    private Record oldRecord;

    public Record(PurchaseRecord purchaseRecord) {
        this.purchaseRecord = purchaseRecord;
    }

    public PurchaseRecord getPurchaseRecord() {
        return purchaseRecord;
    }

    public void setOldRecord(Record oldRecord) {
        this.oldRecord = oldRecord;
    }

    public Record getOldRecord() {
        return oldRecord;
    }

    public void process() {
        calculateValidityDays();
    }

    public Boolean getAlreadySwitched() {
        setAlreadySwitched(oldRecord != null && oldRecord.getAlreadySwitched());
        return this.alreadySwitched;
    }

    public void setAlreadySwitched(Boolean alreadySwitched) {
        this.alreadySwitched = alreadySwitched;
    }

    public boolean isSwitched() {
        return !Objects.equals(oldRecord.purchaseRecord.getMedication(), this.purchaseRecord.getMedication());
    }

    public boolean isOverlapped() {
        return purchaseRecord.getDay() < oldRecord.validityDays;
    }

    private void calculateValidityDays() {
        MedicineType medicineType = MedicineType.valueOf(purchaseRecord.getMedication());
        int medicineDays = medicineType.getDays();
        validityDays = purchaseRecord.getDay() + medicineDays;
    }

}
