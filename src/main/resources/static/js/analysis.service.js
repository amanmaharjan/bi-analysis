var Analysis = (function () {

    var _biAnalysisEndpoint = '/api/bi/analysis';

    function Analysis() {
        // No-op
    }

    Analysis.prototype = {
        getBIAnalysis: function (success, failure) {
            $.ajax({
                url: _biAnalysisEndpoint,
                dataType: 'json',
                method: 'get',
                success: function(result) {
                    success(result.data);
                },
                error: function(err) {
                    failure(err.responseJSON.data.errorMessages);
                }
            });
        }
    };

    return new Analysis();
})();


